Command to run this example:
```bash
docker run -it --rm -v $(pwd)/nginx.conf:/etc/nginx/nginx.conf -p 8081:80 nginx
```